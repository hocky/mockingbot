package com.mockingbot.mockingbot.service;

import com.mockingbot.mockingbot.listener.PingPongListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.security.auth.login.LoginException;

@Service
public class MockingBotServiceImpl implements MockingBotService{

    @Value("${jda.discord.token}")
    private String botToken;
    private JDA jda;

    @Override
    public void startBot() throws InterruptedException, LoginException {
        System.out.println(botToken);
        jda = JDABuilder.createDefault(botToken).build();
        jda.addEventListener(new PingPongListener());
        jda.awaitReady();
    }

    @Override
    public JDA getJDA() {
        return jda;
    }
}
