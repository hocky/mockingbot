package com.mockingbot.mockingbot.service;

import net.dv8tion.jda.api.JDA;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.security.auth.login.LoginException;

@Service
public interface MockingBotService {
    void startBot() throws InterruptedException, LoginException;
    JDA getJDA();
}
