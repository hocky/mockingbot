package com.mockingbot.mockingbot;

import com.mockingbot.mockingbot.service.MockingBotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@EnableConfigurationProperties
@SpringBootApplication
public class MockingbotApplication implements CommandLineRunner {

	private final MockingBotService mockingBotService;

	public MockingbotApplication(MockingBotService mockingBotService){
		this.mockingBotService = mockingBotService;
	}

	public static void main(String[] args) {
		SpringApplication.run(MockingbotApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		mockingBotService.startBot();
	}
}
